<?php
/**
 * User: rrafia
 * Date: 01/28/16
 */

namespace Aracademia\DbBackup\Facades;


use Illuminate\Support\Facades\Facade;

class DbBackup extends Facade {

    protected static function getFacadeAccessor()
    {
        return 'DbBackup';
    }

} 