<?php
/**
 * User: rrafia
 * Date: 01/28/16
 */

namespace Aracademia\DbBackup;

use Aracademia\DbBackup\Console\DbBackupCommand;
use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;


class DbBackupServiceProvider extends ServiceProvider {


    public function boot()
    {

        $this->publishes([
            __DIR__.'/Config/DbBackupConfig.php' => config_path('DbBackup.php'),
        ]);
    }

    public function register()
    {
        $this->app['DbBackup'] = $this->app->share(function($app)
        {
            return new DbBackup();
        });
        $this->app['DbBackupCommand'] = $this->app->share(function($app)
        {
            return new DbBackupCommand();
        });

        $this->commands('DbBackupCommand');

        //register our facades
        $this->app->booting(function()
        {
            AliasLoader::getInstance()->alias('DbBackup','Aracademia\DbBackup\Facades\DbBackup');
        });


    }

} 